

public class Microwave{
	//Creating the fields
	private String brand;
	private String color;
	private int maxDuration;
	private String textColor;
		
	public void printBrand(){
		System.out.println(this.brand);
	}
	
	public void heatFood(){	
		
		for (int i=this.maxDuration;i>0;i--){
			System.out.println("Your food is ready in "+i+" seconds" );
		}
		System.out.println("YOUR FOOD IS READY!");
	}
	//Part1
	//Helper method
	private boolean helper(String newTextCol){
		boolean valid;
		switch (newTextCol.toLowerCase()){
			case "green": 
			valid = true;
			break;
			case "blue":
			valid = true;
			break;
			default:
			valid=false;
		}
		return valid;
	}
	
	public void changeTextColor(String newTextCol)
	{
		
		if (helper(newTextCol)){
		this.textColor = newTextCol;
		System.out.println("Text color changed to "+newTextCol);
		}
		else 
			System.out.println("Can't switch to this color");

	}
	//PART 2
	
	public String getBrand()
	{
		return this.brand;
	}
	public void setBrand(String newBrand)
	{
		this.brand = newBrand;
	}
	public String getColor()
	{
		return this.color;
	}
	public void setColor(String newColor)
	{
		this.color = newColor;
	}
	public int getMaxDuration()
	{
		return this.maxDuration;
	}
	public void setMaxDuration(int newMaxDuration)
	{
		this.maxDuration = newMaxDuration;
	}
	public String getTextColor()
	{
		return this.textColor;
	}
	
	//PART 3
	public Microwave(String brand, String color, int MaxDuration){
		
		this.brand = brand;
		this.color = color;
		this.maxDuration = maxDuration;
		
	}
	

	

}
